# Model Training on AWS Sagemaker

## Introduction
This document is specifically for training the model on AWS Sagemaker.

## Directory Structure
- <PROJECT_ROOT>
    - config/
    - docs/
    - dataset_downloader/
    - helper/
    - model_data/
    - Dockerfile_AWS
    - Dockerfile_Alibaba
    - Dockerfile_Local_training
    - train_model.py
    - dataset_downloader.py
    - README_Dataset_downloading.md
    - README_AliCloud_Model_training_setup.md
    - README_AWS_Model_training_setup.md
    - README_Local_Model_training_setup.md

## PREREQUISITES 

- Docker is installed in host system(x86 system). 
- Good internet connection. 
- Minimum of 50 GB of storage available in host system. 
- AWS Account with Sagemaker, ECR , S3 services enabled. 
  - For creating of AWS Account refer to this [doc](https://docs.aws.amazon.com/accounts/latest/reference/manage-acct-creating.html). 
  - For creating the IAM user refer to this [doc](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_users_create.html). 
  - For creating the Access keys and secret keys refer to this [doc](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_credentials_access-keys.html). 
# Overview
- Dependencies Installation
- Clone the repository
- Training the model on Sagemaker
- What's next ?

## Dependencies Installation
Before doing installation setup, the user needs to set up the [AWS Account](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-prereqs.html).


- Setting up the AWS-CLI using below commands
```sh
curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
./aws/install
```
- For setting up the AWS CLI Completely follow the instructions on the [AWS-CLI Documentation](https://docs.aws.amazon.com/cli/latest/userguide/getting-started-install.html).

- To install AWS Python SDK, run the command below
```
python3 -m pip install boto3
```

- Setup the S3 bucket by following instructions on [Setup the S3 bucket](https://docs.aws.amazon.com/AmazonS3/latest/userguide/creating-bucket.html).

## Training the model on Sagemaker

### Creating the Docker image on Host
This section guides the user to train the model on AWS Sagemaker.
- Build a docker container on the Host-System (Docker should be
installed) for training the model, using a Dockerfile, provided in the
project folder.
```sh
cd <PROJECT_ROOT>
```
- Open the config file **cloud_config/aws_config.json** in an editor.
    - Update the aws preferred region id, access key and secret key.
    - Update the AWS S3 bucket name into which the trained model will be saved.
    - Save the file.
- Open the Dockerfile_AWS in an editor and update the parameters in the last line.
    The parameters are explained below
    - -m: Type of model user wants to train. Supported models are
`yolov3/tiny_yolov3`.
    - -c: Cloud platform that the user is training the model on. Supported
platforms are AWS/Alibaba.
    - -e: Number of epochs the model should be trained on. The preferred value would be 1500.

- Build the docker container using below command
    - The user needs to have a good internet connection as the image will download the dataset.
    ```sh
    sudo docker build -t <IMAGE_NAME>:<IMAGE_TAG> -f Dockerfile_AWS .
    ```

        Users can specify the IMAGE_NAME & TAG_NAME of their choice. IMAGE_NAME is the name of the docker image. TAG_NAME is to tag the different variants of the same image.

### Creating the Docker Repository on AWS ECR
- Please go to the AWS Console and search for the ECR.
- Once search result appears, Click on **Elastic Container Registry**.
- If you are new on the ECR Console, please click on “Get Started” to create the repository, and give it the name as "arm_yolov3_training". Keep remaining fields unchanged.
- Click on **Create Repository**.
- To start the training on Sagemaker, the docker image needs to be uploaded in AWS ECR.

### Pushing the docker image on AWS ECR
- In ECR, after creating a repository, Click on **View push commands**.
    - Follow the instructions given on the page to push the image.
    - Run the described commands on the host machine to push the image to ECR.

### Creating the IAM Role for Model training
    - Create a role for Sagemaker training jobs.
The role will give Sagemaker permission to access all the necessary resources. This policy gives Sagemaker access to S3 buckets and ECR images.
    - For further information on how to create a service role,
read [Creating a role to delegate permissions to an AWS service](https://docs.aws.amazon.com/IAM/latest/UserGuide/id_roles_create_for-service.html).

### Configuring the Training Job
To train a model with Amazon Sagemaker, users need to create a training job.
- The job name and role name need to be mentioned and the container ECR path needs to be provided, where the user has pushed the docker image.
- Specify resource configurations
- And finally, you need to specify the output data configuration path.

    Output data configuration is the path for S3 Bucket where trained model artifacts will be saved.
- Users can now monitor the status of the job from the AWS console, where the training logs and instance metrics can also be seen.
- After the training job is finished, model weights stored in the output S3 bucket can be downloaded.

## What's next ?
- Refer to the Model compilation repository README document.

