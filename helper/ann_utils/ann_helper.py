"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
def decode_class_name(classes_path):
    with open(classes_path, 'r') as f:
        lines = f.readlines()
    classes = []
    for line in lines:
        line = line.strip()
        if line:
            classes.append(line)
    return classes


def read_annotation(anno_path, type='y_true'):
    with open(anno_path, 'r') as f:
        lines = f.readlines()
    anno = []
    for line in lines:
        line = line.strip()
        if line:
            anno.append(read_line(line, type))
    return anno


def read_line(line, type):
    if type == 'y_true':
        return read_line_y_true(line)
    elif type == 'y_pred':
        return read_line_y_pred(line)
    

def read_line_y_pred(line):
    
    anns = line.split()
    path = anns[0]
    anns = anns[1:]

    bboxes = []
    labels = []
    confis = []
    for ann in anns:
        if not ann:
            continue
        x1, y1, x2, y2, label, confi = ann.split(',')
        x1, y1, x2, y2, label, confi = float(x1), float(y1), float(x2), float(y2), int(label), float(confi)
        bboxes.append([x1, y1, x2, y2])
        labels.append(label)
        confis.append(confi)

    return path, bboxes, labels, confis


def read_line_y_true(line):
    
    anns = line.split()
    path = anns[0]
    anns = anns[1:]

    bboxes = []
    labels = []
    for ann in anns:
        if not ann:
            continue
        x1, y1, x2, y2, label = ann.split(',')
        x1, y1, x2, y2, label = float(x1), float(y1), float(x2), float(y2), float(label)
        bboxes.append([x1, y1, x2, y2])
        labels.append(label)

    return path, bboxes, labels
