"""
Copyright © 2023 Arm Ltd and Contributors. All rights reserved.
SPDX-License-Identifier: Apache-2.0
"""
# -*- coding: utf-8 -*-
import numpy as np

from tensorflow.keras.utils import Sequence
from helper.dataset_utils import augment_data
from helper.image_utils.img_helper import read_img, preprocess_img
from helper.ann_utils.ann_helper import read_annotation, decode_class_name


class Create_Dataset(Sequence):

    def __init__(self, mask, anchors, max_outputs, strides, name_path, anno_path, img_size, batch_size, normal_method=True, mosaic=False, groundTruth_smoothing=False, verbose=0):

        self.verbose = verbose

        self.mask = mask
        self.anchors = anchors
        self.max_boxes = max_outputs
        self.strides = strides

        self.name_path = name_path
        self.anno_path = anno_path
        self.img_size = img_size
        self.batch_size = batch_size

        self.normal_method = normal_method
        self.mosaic = mosaic
        self.groundTruth_smoothing = groundTruth_smoothing

        self.annotation = read_annotation(anno_path=self.anno_path)
        self.num_anno = len(self.annotation)
        self.name = decode_class_name(self.name_path)
        self.num_classes = len(self.name)

        # init
        self._img_size = np.random.choice(self.img_size)
        self._grid_size = self._img_size // self.strides

    def __len__(self):
        return int(np.ceil(float(len(self.annotation)) / self.batch_size))

    def __getitem__(self, idx):

        l_bound = idx * self.batch_size
        r_bound = (idx + 1) * self.batch_size

        if r_bound > len(self.annotation):
            r_bound = len(self.annotation)
            l_bound = r_bound - self.batch_size

        self._on_batch_start(idx)

        batch_img = np.zeros((r_bound - l_bound, self._img_size, self._img_size, 3), dtype=np.float32)
        batch_groundTruth = [np.zeros((r_bound - l_bound, size, size, len(mask_per_layer) * (5 + self.num_classes)),
                                dtype=np.float32)
                       for size, mask_per_layer in zip(self._grid_size, self.mask)]

        for i, sub_idx in enumerate(range(l_bound, r_bound)):
            img, bound_boxes, groundTruths = self._getitem(sub_idx)

            if self.mosaic:
                sub_idx = np.random.choice(np.delete(np.arange(self.num_anno), idx), 3, False)
                img2, bound_boxes2, groundTruths2 = self._getitem(sub_idx[0])
                img3, bound_boxes3, groundTruths3 = self._getitem(sub_idx[1])
                img4, bound_boxes4, groundTruths4 = self._getitem(sub_idx[2])
                img, bound_boxes, groundTruths = augment_data.augment_mosic(img, bound_boxes, groundTruths,
                                                      img2, bound_boxes2, groundTruths2,
                                                      img3, bound_boxes3, groundTruths3,
                                                      img4, bound_boxes4, groundTruths4)
            if self.normal_method:
                img = augment_data.create_random_distort(img)
                img = augment_data.create_random_grayscale(img)
                img, bound_boxes = augment_data.create_random_flip_lr(img, bound_boxes)
                img, bound_boxes = augment_data.create_random_rotate(img, bound_boxes)
                img, bound_boxes, groundTruths = augment_data.create_random_crop_and_zoom(img, bound_boxes, groundTruths,
                                                                     (self._img_size, self._img_size))

            img, bound_boxes, groundTruths = augment_data.filter_bboxes(img, bound_boxes, groundTruths)
            groundTruths = self._preprocess_true_boxes(bound_boxes, groundTruths)

            batch_img[i] = img
            for j in range(len(self.mask)):
                batch_groundTruth[j][i, :, :, :] = groundTruths[j][:, :, :]

        return batch_img, batch_groundTruth

    def _getitem(self, sub_idx):
        path, bound_boxes, groundTruths = self.annotation[sub_idx]
        img = read_img(path)

        if len(bound_boxes) != 0:
            bound_boxes, groundTruths = np.array(bound_boxes), np.array(groundTruths)
        else:
            bound_boxes, groundTruths = np.zeros((0, 4)), np.zeros((0,))

        img, bound_boxes = preprocess_img(img, (self._img_size, self._img_size), bound_boxes)
        groundTruths = augment_data.create_onehot(groundTruths, self.num_classes, self.groundTruth_smoothing)

        return img, bound_boxes, groundTruths

    def _preprocess_true_boxes(self, bound_boxes, groundTruths):

        bound_boxes_groundTruth = [np.zeros((size, size, len(mask_per_layer), 5 + self.num_classes), np.float32)
                        for size, mask_per_layer in zip(self._grid_size, self.mask)]

        bound_boxes = np.array(bound_boxes, dtype=np.float32)
        # calculate anchor index for true boxes
        anchor_area = self.anchors[:, 0] * self.anchors[:, 1]
        bound_boxes_wh = bound_boxes[:, 2:4] - bound_boxes[:, 0:2]

        bound_boxes_wh_exp = np.tile(np.expand_dims(bound_boxes_wh, 1), (1, self.anchors.shape[0], 1))
        boxes_area = bound_boxes_wh_exp[..., 0] * bound_boxes_wh_exp[..., 1]
        intersection = np.minimum(bound_boxes_wh_exp[..., 0], self.anchors[:, 0]) * np.minimum(bound_boxes_wh_exp[..., 1],
                                                                                          self.anchors[:, 1])
        iou = intersection / (boxes_area + anchor_area - intersection + 1e-8)  # (N, A)
        best_anchor_idxs = np.argmax(iou, axis=-1)  # (N,)

        for i, bbox in enumerate(bound_boxes):
            
            search = np.where(self.mask == best_anchor_idxs[i])
            best_detect = search[0][0]
            best_anchor = search[1][0]

            coord_xy = (bbox[0:2] + bbox[2:4]) * 0.5
            coord_xy /= self.strides[best_detect]
            coord_xy = coord_xy.astype(np.int)

            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, :4] = bbox
            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, 4:5] = 1.
            bound_boxes_groundTruth[best_detect][coord_xy[1], coord_xy[0], best_anchor, 5:] = groundTruths[i, :]

        return [layer.reshape([layer.shape[0], layer.shape[1], -1]) for layer in bound_boxes_groundTruth]

    def _on_batch_start(self, idx, patience=10):
        if idx % patience == 0:
            self._img_size = np.random.choice(self.img_size)
            self._grid_size = self._img_size // self.strides

            if self.verbose:
                print('Change img size to', self._img_size)

    def on_epoch_end(self):
        np.random.shuffle(self.annotation)  # shuffle


def decode_class_names(name_path):
    with open(name_path, 'r') as f:
        lines = f.readlines()
    name = []
    for line in lines:
        line = line.strip()
        if line:
            name.append(line)
    return name
